const pBatu = document.getElementById("pBatu");
const pKertas = document.getElementById("pKertas");
const pGunting = document.getElementById("pGunting");

const comBatu = document.getElementById("comBatu");
const comKertas = document.getElementById("comKertas");
const comGunting = document.getElementById("comGunting");

const matchResult = document.getElementById("matchResult");

const allDivIcon = document.querySelectorAll("div");

const selections = {
  Batu: { name: "Batu", defeat: "Gunting" },
  Kertas: { name: "Kertas", defeat: "Batu" },
  Gunting: { name: "Gunting", defeat: "Kertas" },
};

// Get Pilihan Computer Secara Acak
function getComputerChoice() {
  const choices = ["Batu", "Kertas", "Gunting"];
  const randomNumber = Math.floor(Math.random() * 3);
  return choices[randomNumber];
}

// Reset Pilihan Player, Supaya warna pilihan kembali ke awal
function resetSelected() {
  allDivIcon.forEach((div) => {
    div.classList.remove(
      "selected",
      "score-card",
      "score-transition",
      "text-xl"
    );
  });
}

// Restart the Game
function restart() {
  resetSelected();
  matchResult.textContent = "VS";
  matchResult.classList.add("md:text-7xl", "tracking-widest", "text-red-700");
}

// COM Styling after Select
function displayComputerSelect(computerChoice) {
  switch (computerChoice) {
    case "Batu":
      comBatu_div.classList.add("selected");
      break;
    case "Kertas":
      comKertas_div.classList.add("selected");
      break;
    case "Gunting":
      comGunting_div.classList.add("selected");
      break;
  }
}

// Player Selection
function pilih(pilihanPlayer) {
  resetSelected();
  const computerChoice = getComputerChoice();
  displayComputerSelect(computerChoice);
  //   Styling player selection
  switch (pilihanPlayer) {
    case "Batu":
      pBatu_div.classList.add("selected");
      break;
    case "Kertas":
      pKertas_div.classList.add("selected");
      break;
    case "Gunting":
      pGunting_div.classList.add("selected");
      break;
  }
  //   Match Result
  //   console.log(pilihanPlayer, computerChoice);
  const select = selections[pilihanPlayer];
  if (pilihanPlayer === computerChoice) {
    matchResult.textContent = "DRAW";
    matchResult.classList.add("score-card", "score-transition", "text-xl");
    matchResult.classList.remove(
      "md:text-7xl",
      "tracking-widest",
      "text-red-700"
    );
  } else if (select.defeat.indexOf(computerChoice) > -1) {
    matchResult.textContent = "PLAYER 1 WIN";
    matchResult.classList.add("score-card", "score-transition", "text-xl");
    matchResult.classList.remove(
      "md:text-7xl",
      "tracking-widest",
      "text-red-700"
    );
  } else {
    matchResult.textContent = "COM WIN";
    matchResult.classList.add("score-card", "score-transition", "text-xl");
    matchResult.classList.remove(
      "md:text-7xl",
      "tracking-widest",
      "text-red-700"
    );
  }
}
