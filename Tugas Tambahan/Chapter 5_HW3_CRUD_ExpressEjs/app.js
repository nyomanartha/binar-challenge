// Import
const express = require("express");
const app = express();
const { router } = require("./routes/router");
const port = 8000;

// Set views
app.set("view engine", "html");
app.engine("html", require("ejs").renderFile);

//route
app.use(router);

// Listen to port
app.listen(port, () => console.log(`server is running at port ${port}`));
