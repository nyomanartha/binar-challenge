let todoList = [];

// Create todo
function addTodoList(todo) {
  todoList.unshift(todo);
}

// Read todo
function getTodoList() {
  console.log("List Todo:", todoList);
  console.log("\n");
}

// Update todo status
function updateStatus(id) {
  todoList.forEach(function (todo) {
    if (id == todo.id) {
      todo.status = "Done";
    }
  });
}

// Update todo name
function updateTaskName(id, newName) {
  todoList.forEach(function (todo) {
    if (id == todo.id) {
      todo.task_name = newName;
    }
  });
}

// Soft Delete todo
function softDeleteTask(id) {
  todoList.forEach(function (todo) {
    if (id == todo.id) {
      todo.is_deleted = true;
    }
  });
}

// Hard Delete todo
function hardDeleteTask(id) {
  const filteredTask = todoList.filter(function (todo) {
    return todo.id !== id;
  });
  todoList = filteredTask;
}

// Get blank todo
console.log("Todo List Masih Kosong");
getTodoList();

// Add first task
addTodoList({
  id: 1,
  task_name: "Task Pertama",
  status: null,
  is_deleted: false,
});
console.log("Todo List setelah ditambah task pertama");
getTodoList();

// Add second task
addTodoList({
  id: 2,
  task_name: "Task Kedua",
  status: null,
  is_deleted: false,
});
console.log("Todo List setelah ditambah task kedua");
getTodoList();

// Add third task
addTodoList({
  id: 3,
  task_name: "Task Ketiga",
  status: null,
  is_deleted: false,
});
console.log("Todo List setelah ditambah task ketiga");
getTodoList();

// Update null status to done for id 2
console.log("Update Task 2 as Done");
updateStatus(2);
getTodoList();

// Update task 3 name
console.log("Update Task 3 Name");
updateTaskName(3, "Nama sudah berubah");
getTodoList();

// Soft Delete task 2
console.log("Soft delete task 2");
softDeleteTask(2);
getTodoList();

// Hard Delete Task 1
console.log("Delete Task 1, menyisakan task 2 dan 3");
hardDeleteTask(1);
getTodoList();
