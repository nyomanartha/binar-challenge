// Import
const express = require("express");
const { authLogin, getLogin } = require("./auth.service");
const authRouter = express.Router();

authRouter.post("/login", authLogin);
authRouter.get("/login", getLogin);

module.exports = { authRouter };
