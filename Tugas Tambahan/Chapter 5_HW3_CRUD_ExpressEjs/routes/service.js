// Get aka Read dalam CRUD
const getData = (req, res) => {
  // return res.send("Ini adalah GET");
  res.render("read_get");
};

const postData = (req, res) => {
  // return res.send("Ini adalah POST");
  res.render("create_post");
};

const putData = (req, res) => {
  // return res.send("Ini adalah PUT");
  res.render("update_put");
};

const deleteData = (req, res) => {
  // return res.send("Ini adalah DELETE");
  res.render("delete");
};

module.exports = { getData, postData, putData, deleteData };
