const fs = require("fs");
const process = require("process");
const args = process.argv;

// ===========================================================================================
// ===========================================================================================
// ===========================================================================================

// Ambil semua data di file recipe.json
function getRecipes() {
  const recipes = fs.readFileSync("./recipe.json", "utf-8");
  return JSON.parse(recipes);
}

// Ambil data spesifik berdasarkan nama dari recipe.json
function getDetailRecipes(nama) {
  const recipes = getRecipes();
  recipes.forEach((recipe) => {
    if (recipe.nama === nama) {
      console.log("recipe", recipe);
    }
  });
}

// Menambahkan recipe baru
function addRecipes(newRecipe) {
  const recipes = getRecipes();
  recipes.push(newRecipe);
  console.log(recipes);
  fs.writeFileSync("./recipe.json", JSON.stringify(recipes));
}

// Delete recipe berdasarkan nama
function deleteRecipe(nama) {
  const recipes = getRecipes();
  const recipesFiltered = recipes.filter((recipe) => {
    return recipe.nama !== nama;
  });
  console.log("recipesFiltered", recipesFiltered);
  fs.writeFileSync("./recipe.json", JSON.stringify(recipesFiltered));
}

// Update recipe
function updateRecipe(nama) {
  const recipes = getRecipes();
  const data = {
    nama: args[4],
    origin: args[5],
    harga: args[6],
    review: args[7],
  };
  const hasil = recipes.map((recipe) => {
    if (nama === recipe.nama) {
      return data;
    } else {
      return recipe;
    }
  });
  fs.writeFileSync("./recipe.json", JSON.stringify(hasil));
}

// // List semua recipes
// if (args[2] == "list") {
//   console.log("list all recipe");
//   const recipes = getRecipes();
//   console.log("Recipes", recipes);
// }

// // List spesifik recipe berdasarkan nama
// if (args[2] == "detail") {
//   getDetailRecipes(args[3]);
// }

// // Create recipe baru dengan urutan nama origin harga review
// if (args[2] == "create") {
//   const newRecipe = {
//     nama: args[3],
//     origin: args[4],
//     harga: args[5],
//     review: args[6],
//   };
//   //   console.log("movie", movie);
//   addRecipes(newRecipe);
// }

// // Update recipe baru dengan urutan nama origin harga review
// if (args[2] == "update") {
//   updateRecipe(args[3]);
// }

// // delete recipe berdasarkan nama
// if (args[2] == "delete") {
//   deleteRecipe(args[3]);
// }

// List semua recipes
function list() {
  console.log("list all recipe");
  const recipes = getRecipes();
  console.log("Recipes", recipes);
}

// List spesifik recipe berdasarkan nama
function detail() {
  getDetailRecipes(args[3]);
}

// Create recipe baru dengan urutan nama origin harga review
function create() {
  const newRecipe = {
    nama: args[3],
    origin: args[4],
    harga: args[5],
    review: args[6],
  };
  //   console.log("movie", movie);
  addRecipes(newRecipe);
}

// Update recipe baru dengan urutan nama origin harga review
function update() {
  updateRecipe(args[3]);
}

// delete recipe berdasarkan nama
function deleteFn() {
  deleteRecipe(args[3]);
}

const actions = {
  list,
  detail,
  create,
  update,
  delete: deleteFn,
};

actions[args[2]]();

// ==============================================================
// Notes & List Command
// List all Recipes = node app.js list
// Read Spesifik Recipe = node app.js detail ['nama recipe]
// Create New Recipes = node app.js create ['nama recipe' 'origin' 'harga' 'review']
// Update  Recipes = node app.js update ['nama recipe lama' 'nama recipe baru' 'origin' 'harga' 'review']
// Delete Recipes = node app.js delete ['nama recipe yang mau dihapus']
