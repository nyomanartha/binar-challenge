// Import
const express = require("express");
const { getData, postData, putData, deleteData } = require("./service");
const router = express.Router();

router.get("/", getData);
router.post("/", postData);
router.put("/", putData);
router.delete("/", deleteData);

module.exports = { router };
