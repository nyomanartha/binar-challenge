const fs = require("fs");
const process = require("process");
const args = process.argv;

// ===========================================================================================
// ===========================================================================================
// ===========================================================================================

// // Ambil semua data di file recipe.json
// function getRecipes() {
//   // const recipes = fs.readFile("recipe.json", "utf-8");
//   // return JSON.parse(recipes);
//   fs.readFile("recipe.json", "utf-8", (err, data) => {
//     console.log(data);
//   });
// }

let getRecipes = require("./recipe.json");

function readRecipes() {
  fs.readFile("recipe.json", "utf-8", (err, data) => {
    if (err) throw err;
    readRecipes = JSON.parse(data);
    console.log(readRecipes);
  });
}
// Ambil data spesifik berdasarkan nama dari recipe.json
function getDetailRecipes(nama) {
  const recipes = getRecipes;
  recipes.forEach((recipe) => {
    if (recipe.nama === nama) {
      console.log("recipe", recipe);
    }
  });
}

// Menambahkan recipe baru
function addRecipes(newRecipe) {
  const recipes = getRecipes;
  recipes.push(newRecipe);
  console.log(recipes);
  let data = JSON.stringify(recipes);
  fs.writeFile("./recipe.json", data, (err) => {
    if (err) throw err;
    console.log("data written to file");
  });
}

// Delete recipe berdasarkan nama
function deleteRecipe(nama) {
  const recipes = getRecipes;
  const recipesFiltered = recipes.filter((recipe) => {
    return recipe.nama !== nama;
  });
  console.log("recipesFiltered", recipesFiltered);
  let data = JSON.stringify(recipesFiltered);
  fs.writeFile("./recipe.json", data, (err) => {
    if (err) throw err;
    console.log("data deleted");
  });
}

// Update recipe
function updateRecipe(nama) {
  const recipes = getRecipes;
  const data = {
    nama: args[4],
    origin: args[5],
    harga: args[6],
    review: args[7],
  };
  const hasil = recipes.map((recipe) => {
    if (nama === recipe.nama) {
      return data;
    } else {
      return recipe;
    }
  });
  let dataUpdate = JSON.stringify(hasil);
  fs.writeFile("./recipe.json", dataUpdate, (err) => {
    if (err) throw err;
    console.log("data updated");
  });
}

// List semua recipes
if (args[2] == "list") {
  console.log("list all recipe");
  console.log(readRecipes());
}

// List spesifik recipe berdasarkan nama
if (args[2] == "detail") {
  getDetailRecipes(args[3]);
}

// Create recipe baru dengan urutan nama origin harga review
if (args[2] == "create") {
  const newRecipe = {
    nama: args[3],
    origin: args[4],
    harga: args[5],
    review: args[6],
  };
  //   console.log("movie", movie);
  addRecipes(newRecipe);
}

// Update recipe baru dengan urutan nama origin harga review
if (args[2] == "update") {
  updateRecipe(args[3]);
}

// delete recipe berdasarkan nama
if (args[2] == "delete") {
  deleteRecipe(args[3]);
}

// ==============================================================
// Notes & List Command
// List all Recipes = node app.js "list"
// Read Spesifik Recipe = node app.js "detail" ['nama recipe]
// Create New Recipes = node app.js "create" ['nama recipe' 'origin' 'harga' 'review']
// Update  Recipes = node app.js "update" ['nama recipe lama' 'nama recipe baru' 'origin' 'harga' 'review']
// Delete Recipes = node app.js "delete" ['nama recipe yang mau dihapus']
