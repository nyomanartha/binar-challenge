const userData = require("../user/users.json");

// Membuat kondisi login, dimana apabila username dan password sesuai maka mendapatkan hasil yang positif
const authLogin = (req, res) => {
  const username = req.headers.username;
  const password = req.headers.password;

  const selectedUser = userData.find(
    (index) => index.username == username && index.password == password
  );

  if (selectedUser) {
    res
      .status(200)
      .json({ status: 200, message: "Login success!", data: selectedUser });
  } else {
    res.status(400).json({
      status: 400,
      message:
        "Login failed! Please check your username was registered or your password filled correctly!",
    });
  }
};

// Membuat message apabila request get dan bukan post
const getLogin = (req, res) => {
  return res.send(
    "Tolong gunakan Post dan bukan Get. dan Pastikan tambahkan username dan password pada headers dengan key 'username' dan 'password' menggunakan postman atau insomnia untuk bisa login"
  );
};

module.exports = { authLogin, getLogin };
