// Import
const express = require("express");
const app = express();
const morgan = require("morgan");
const { authRouter } = require("./auth/auth.router");
const port = 8000;

//set respond json
app.use(express.json());

// Static Files
app.use(express.static("public"));
app.use("/css", express.static(__dirname + "public/css"));
app.use("/js", express.static(__dirname + "public/js"));
app.use("/assets", express.static(__dirname + "public/assets"));

// Set views to use html format
app.set("view engine", "html");
app.engine("html", require("ejs").renderFile);

//use morgan as middleware
app.use(morgan("tiny"));

// route for index.html aka challenge chapter 3
app.get("/", (req, res) => {
  res.render("index");
});

// route for  game.html aka challenge chapter 4
app.get("/game", (req, res) => {
  res.render("game");
});

//route to login
app.use(authRouter);

// Listen to port
app.listen(port, () => console.log(`server is running at port ${port}`));
